<?php
function randomPassword($length = 16) {
    $alphabet = "abcdefghijklmnopqrstuwxyz";
    $alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
    $numbers = "0123456789";

    $char_length = round($length/3);

    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 1; $i < $char_length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }

    $numberlength = strlen($numbers) - 1;
    for($n = 0; $n <= $char_length; $n++) {
        $i = rand(0, $numberlength);
        $pass[] = $numbers[$i];
    }

    $alphabetUpperLength = strlen($alphabetUpper) - 1;
    for($c = 0; $c < $char_length; $c++) {
        $r = rand(0, $alphabetUpperLength);
        $pass[] = $alphabetUpper[$r];
    }

    $password_length = count($pass);
    $remaining_characters = $length-$password_length;

    for($needrest = 0; $needrest < $remaining_characters; $needrest++) {
        $fillup = rand(0, $alphaLength);
        $pass[] = $alphabet[$fillup];
    }

    shuffle($pass); shuffle($pass); shuffle($pass); //Seems legit enough.

    return implode($pass);
}

define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/includes/password.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$username = "sharksmedia";
$email = "allan@sharksmedia.dk";
$password = randomPassword();

if(user_load_by_mail($email) == false && user_load_by_name($username) == false) {
    $new_user = (object) array(
        'name' => $username,
        'pass' => user_hash_password($password),
        'mail' => $email,
        'is_new' => true,
        'status' => 1,
        'init' => $email,
        'roles' => array(
            DRUPAL_AUTHENTICATED_RID => 'authenticated user',
            3 => 'administrator',
        ),
    );

	user_save($new_user);

	print "Username: ".$username."<br>";
	print "Password: ".$password."<br>";
	print "Email: ".$email."<br>";
	print "Uid: ".$new_user->uid;
} else {
	print "User not created, username or email already in use.";
}

?>