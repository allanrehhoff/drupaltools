<?php
/*
* Find all nodes
*/
ini_set('display_errors', 1);
error_reporting(E_ALL);

define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/includes/password.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

restore_error_handler();

$nodesQ = db_query('SELECT nid FROM node')->fetchAll();

if(!module_exists('php')) die('PHP filter not enabled... abort');

$potential_fields = array('body');
$could_be_malicious = array();
foreach($nodesQ as $n) {
	$node = node_load($n->nid);

	foreach($potential_fields as $field) {
		if(!empty($node->$field)) {
			if($node->{$field}[LANGUAGE_NONE][0]['format'] == 'php_code') {
				$could_be_malicious[] = $node->nid;
				break;
			}
		}
	}
}
if(empty($could_be_malicious)) {
	echo "No nodes with PHP.";
} else {
	print implode(',', $could_be_malicious);
}
?>