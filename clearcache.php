<?php
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	restore_error_handler();

	// define static var
	define('DRUPAL_ROOT', getcwd());
	// include bootstrap
	include_once('./includes/bootstrap.inc');
	// initialize stuff
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	// clear cache
	db_query("DELETE FROM cache_bootstrap");
	db_query("DELETE FROM cache_field");
	db_query("DELETE FROM cache_form");
	db_query("DELETE FROM cache_menu");
	db_query("DELETE FROM cache_block");
	db_query("DELETE FROM cache_filter");
	db_query("DELETE FROM cache_image");
	db_query("DELETE FROM cache_page");
	db_query("DELETE FROM cache_path");
	db_query("DELETE FROM cache");
	//drupal_flush_all_caches();

	echo "Caches cleared!";
?>