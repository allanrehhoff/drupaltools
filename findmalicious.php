<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);
restore_error_handler();

class phpMalCodeScan {
	public $infected_files = array();
	private $scanned_files = array();
	
	
	public function __construct() {
		$this->scan(dirname(__FILE__));
	}
	
	
	public function scan($dir) {
		$this->scanned_files[] = $dir;
		$files = scandir($dir);
		
		if(!is_array($files)) {
			throw new Exception('Unable to scan directory ' . $dir . '.  Please make sure proper permissions have been set.');
		}
		
		foreach($files as $file) {
			if(is_file($dir.'/'.$file) && !in_array($dir.'/'.$file,$this->scanned_files)) {
				$this->check($dir.'/'.$file);
			} elseif(is_dir($dir.'/'.$file) && substr($file,0,1) != '.') {
				$this->scan($dir.'/'.$file);
			}
		}
	}
	
	public function check($file) {
		$this->scanned_files[] = $file;
		//$contents = file_get_contents($file);

		$line_matches = preg_grep('/eval\((base64|_stop|eval|\$_|\$\$|\$[A-Za-z_0-9\{]*(\(|\{|\[))/i', file($file));
		if(!empty($line_matches)) {
			$infected_file = array(
				'matches' => $line_matches,
				'filename' => $file
			);

			$this->infected_files[] = $infected_file;
		}
	}

	public function showResults() {
		$infected_files_html = '';

		foreach($this->infected_files as $f) {
			$matches = array();

			foreach($f['matches'] as $line => $match) {
				$match_line =  htmlentities(trim($match));
				// +1 to match the actual line because array indexes start at 0 and file lines start 1 one
				$matches[] = '<strong>Line: '.($line+1).'</strong> '.$match_line;
			}

			$infected_files_html .= '<div style="background:#ECECEC; padding:10px; border:1px solid grey; margin-top:10px;">';
				$infected_files_html .= '<strong>Filename:</strong> '.$f['filename'].'<br>';
				$infected_files_html .= implode('<br>', $matches);
			$infected_files_html .= '</div>';
		}

		$html = '
			<a href="#" onclick="show_scanned_files();">Show scanned files</a> - <a href="#" onclick="show_infected_files();">Show matched files</a><br>
			<div id="scanned_files" style="display:none;">
				'.implode('<br>', $this->scanned_files).'
			</div>
			<div id="infected_files">
				'.$infected_files_html.'
			</div>
		';

		return $html;
	}

}

ini_set('memory_limit', '-1'); ## Avoid memory errors (i.e in foreachloop)
$scanner = new phpMalCodeScan;


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Malicious code scanner</title>
	<style>
		a {
			color:rgb(0,122,255);
		}
		body {
			font-family:arial;
			font-size:14px;
		}
		h1 {
			margin-top:0px;
		}
		.notice {
			margin-bottom:20px;
		}
	</style>
</head>
<body>
	<div class="notice">
		<h1>Important notice</h1>
		Do not trust this script blindly, it was written to only match common backdoors. <br>
		False positives may also occur in the list.
	</div>
	<?php print $scanner->showResults(); ?>	
	<script type="text/javascript">
		function show_scanned_files() {
			document.getElementById('infected_files').style.display = 'none';
			document.getElementById('scanned_files').style.display = 'block';
		}

		function show_infected_files() {
			document.getElementById('infected_files').style.display = 'block';
			document.getElementById('scanned_files').style.display = 'none';
		}
	</script>
</body>
</html>
