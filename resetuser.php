<?php
/*
* Use this file for resetting the administrative password (or any other users password) by uid.
* Be careful when running this script, it does not ask twice.
* @param pass The new password to be hashed
* @param uid  The users uid to reset password for
* @param name The new username for the user matching uid
* @author Allan Thue Rehhoff
*/

ini_set('display_errors', 1);
error_reporting(E_ALL);
restore_error_handler();

function randomPassword($length = 16) {
    $alphabet = "abcdefghijklmnopqrstuwxyz";
    $alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
    $numbers = "0123456789";

    $char_length = round($length/3);

    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 1; $i < $char_length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }

    $numberlength = strlen($numbers) - 1;
    for($n = 0; $n <= $char_length; $n++) {
        $i = rand(0, $numberlength);
        $pass[] = $numbers[$i];
    }

    $alphabetUpperLength = strlen($alphabetUpper) - 1;
    for($c = 0; $c < $char_length; $c++) {
        $r = rand(0, $alphabetUpperLength);
        $pass[] = $alphabetUpper[$r];
    }

    $password_length = count($pass);
    $remaining_characters = $length-$password_length;

    for($needrest = 0; $needrest < $remaining_characters; $needrest++) {
        $fillup = rand(0, $alphaLength);
        $pass[] = $alphabet[$fillup];
    }

    shuffle($pass); shuffle($pass); shuffle($pass); //Seems legit enough.

    return implode($pass);
}

$username_changed = false;
$change_admin_name_to = 'sharksmedia';

define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/includes/password.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


$rawpass = isset($_GET['pass']) ? $_GET['pass'] : randomPassword();
$fields = array();
$fields['pass'] = user_hash_password($rawpass);
if(isset($_GET['name'])) $fields['name'] = $_GET['name'];
if(isset($_GET['mail'])) $fields['mail'] = $_GET['mail'];
$uid = isset($_GET['uid']) ? $_GET['uid'] : 1;

$u = user_load($uid);

if($u->name == 'admin' || (isset($fields['name']) && $fields['name'] == 'admin')) {
    $fields['name'] = $change_admin_name_to;
    $username_changed = true;
}

$updatepass = db_update('users')
    ->fields($fields)
    ->condition('uid', $uid, '=')
    ->execute();

if($username_changed == true) {
    print "<div style='color:rgb(0, 122, 255);'>Username was changed from 'admin' to '".$change_admin_name_to."' </div>";
}
print "Username: ".($username_changed ? $change_admin_name_to : $u->name)."<br>";
print "Password: ".$rawpass."<br>";
print "Email: ".$u->mail."<br>";
print "Uid: ".$uid."<br>";
if(unlink(__FILE__)) {
    print "<strong>This file has been destroyed automatically.</strong>";
} else {
    print "<strong style='color:red;'>Please delete this file immediately!</strong>";
}

drupal_exit();
?>