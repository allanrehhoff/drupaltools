<?php
	define('DRUPAL_ROOT', getcwd());
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/includes/password.inc';
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

	db_query("UPDATE variable SET value = 'standard' WHERE name = 'install_profile'");
	db_query("UPDATE system SET status = 1 WHERE filename = 'profiles/standard/standard.profile'");
	variable_set('install_profile', 'standard');

	print "Switched to standard profile!";

	drupal_exit();
?>