<?php
	print '<pre>';
	define('DRUPAL_ROOT', getcwd());
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/includes/password.inc';
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

	$options = array();
	foreach (module_list(FALSE, FALSE, TRUE) as $module) {
		print_r($module);
		print '<br>';
		// Drupal 6
		// if ($permissions = module_invoke($module, 'perm')) {
		//  print_r($permissions);
		// }

		// Drupal 7
		if ($permissions = module_invoke($module, 'permission')) {
			print_r($permissions);
		}
	}
	print '<pre>';
?>